class CreateComplains < ActiveRecord::Migration[5.0]
  def change
    create_table :complains do |t|
      t.string :name
      t.integer :user_id
      t.string :devise
      t.string :complain

      t.timestamps
    end
  end
end
