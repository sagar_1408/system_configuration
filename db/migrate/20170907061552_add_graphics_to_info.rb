class AddGraphicsToInfo < ActiveRecord::Migration[5.0]
  def change
    add_column :infos, :graphics_card, :string
  end
end
