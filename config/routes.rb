Rails.application.routes.draw do
  resources :complains
  get 'admin_user/index'
  get 'admin_user/change_pass/:id', to: 'admin_user#change_pass', as: :admin_user_change_pass
  get 'admin_user/new'

  devise_for :users
  devise_scope :user do
    authenticated :user do
      root "infos#index", as: :root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end
  resources :infos do
    get 'download_csv', :on => :collection
    #get "import"
    collection { post :import }
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get "/print_pdf", to: "infos#print_pdf"

  patch '/admin_user/:id/update', to: 'admin_user#update', as: :update_admin_user
  #patch '/user/:id/update', to: 'admin_users#update', as: :update_admin_user
end
