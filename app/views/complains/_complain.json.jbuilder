json.extract! complain, :id, :name, :user_id, :devise, :complain, :created_at, :updated_at
json.url complain_url(complain, format: :json)
