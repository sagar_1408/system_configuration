class AdminUserController < ApplicationController
  def index
    @users = User.all
  end

  def new
  end

  def edit

  end

  def change_pass
    @user =  User.find(params[:id])
  end

  def update
    respond_to do |format|
      @user = User.find(params[:id])
      if @user.update(user_params)
        format.html { redirect_to admin_user_index_path, notice: 'User successfully updated.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { redirect_to root_path }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end
end
