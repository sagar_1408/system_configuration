class InfosController < ApplicationController
  before_action :set_info, only: [:show, :edit, :update, :destroy]

  # GET /infos
  # GET /infos.json
  def index
    if params[:search].present?
      #@infos = Info.find(:all, :conditions => ["name LIKE ? ", "%#{params[:search]}%"])
      @infos = Info.where("(name || monitor || motherboard || processor || ram || hdd || cabinet || keyboard || mouse || mousepad || camera || speaker || headphone ) ILIKE ?", "%#{params[:search]}%").order(:id).paginate(:page => params[:page], :per_page => 10)
      #@infos = Info.search(params[:search]).all
    else
      @infos = Info.all.order(:id).paginate(:page => params[:page], :per_page => 10)
    end
    # respond_to do |format|
    #   format.html
    #   format.pdf do
    #     send_data Info.draw(@infos), :filename => "Sytem_configure.pdf", :type => "application/pdf", :disposition => "inline"
    #   end
    # end
  end

  def print_pdf
    @infos = Info.all.order(:id)
		PDFKit.configure do |config|
  			config.default_options = { :page_size => 'Letter', :margin_top => 15, :margin_left => 15,
    			:margin_right => 15,
    			:footer_right => 'Page [page] / [toPage]', :encoding => 'UTF-8', :print_media_type => true }
		end
		html = render_to_string(:action => "print_pdf.html.erb", :layout => false)
    kit = PDFKit.new(html, :orientation => 'Portrait')
    send_data(kit.to_pdf,:disposition => 'attachment', :filename => "SystemConfiguration.pdf", :type => 'application/pdf')

  end

  # GET /infos/1
  # GET /infos/1.json
  def show
  end

  # GET /infos/new
  def new
    @info = Info.new
  end

  # GET /infos/1/edit
  def edit
  end

  # POST /infos
  # POST /infos.json
  def create
    @info = Info.new(info_params)

    respond_to do |format|
      if @info.save
        format.html { redirect_to @info, notice: 'Info was successfully created.' }
        format.json { render :show, status: :created, location: @info }
      else
        format.html { render :new }
        format.json { render json: @info.errors, status: :unprocessable_entity }
      end
    end
  end

  def download_csv
    @infos = Info.order(:id)
    respond_to do |format|
        format.html
        format.csv{ send_data @infos.to_csv, filename: "System Configuration.csv" }
    end
  end

  def import
    Info.import(params[:file])
    redirect_to root_url, notice: "Data imported successfully..."
  end

  # PATCH/PUT /infos/1
  # PATCH/PUT /infos/1.json
  def update
    respond_to do |format|
      if @info.update(info_params)
        format.html { redirect_to @info, notice: 'Info was successfully updated.' }
        format.json { render :show, status: :ok, location: @info }
      else
        format.html { render :edit }
        format.json { render json: @info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /infos/1
  # DELETE /infos/1.json
  def destroy
    @info.destroy
    respond_to do |format|
      format.html { redirect_to infos_url, notice: 'Info was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_info
      @info = Info.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def info_params
      params.require(:info).permit(:name, :monitor, :motherboard, :processor, :ram, :hdd, :cabinet, :keyboard, :mouse, :mousepad, :camera, :speaker, :headphone, :graphics_card)
    end
end
